from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Dog
from common.json import ModelEncoder


class DogEncoder(ModelEncoder):
    model = Dog
    properties = [
        "id",
        "name",
        "breed",
        "age",
        "weight",
        "picture_url",
        "facebook_url",
        "adopted",
    ]

@require_http_methods(["GET", "POST"])
def api_dogs(request):
    if request.method == "GET":
        dogs = Dog.objects.all()
        return JsonResponse(
            {"dogs": dogs},
            encoder=DogEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            dog = Dog.objects.create(**content)
            return JsonResponse(
                dog,
                encoder=DogEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the dog"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_dog(request, pk):
    if request.method == "GET":
        try:
            dog = Dog.objects.get(id=pk)
            return JsonResponse(
                dog,
                encoder=DogEncoder,
                safe=False
            )
        except Dog.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            dog = Dog.objects.get(id=pk)
            dog.delete()
            return JsonResponse(
                dog,
                encoder=DogEncoder,
                safe=False,
            )
        except Dog.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            dog = Dog.objects.get(id=pk)

            props = ["name"]
            for prop in props:
                if prop in content:
                    setattr(dog, prop, content[prop])
            dog.save()
            return JsonResponse(
                dog,
                encoder=DogEncoder,
                safe=False,
            )
        except Dog.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
