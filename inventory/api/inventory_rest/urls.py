from django.urls import path
from .views import api_dogs, api_dog

urlpatterns = [
    path("dogs/", api_dogs, name="api_dogs"),
    path("dogs/<int:pk>/", api_dog, name="api_dog"),
]
