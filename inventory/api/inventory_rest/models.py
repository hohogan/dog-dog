# from typing import Any
from django.db import models
from django.urls import reverse


class Dog(models.Model):
    name = models.CharField(max_length=100, unique=True)
    breed = models.CharField(max_length=100)
    age = models.CharField(max_length=100)
    weight = models.PositiveSmallIntegerField()
    picture_url = models.URLField()
    facebook_url = models.URLField()
    adopted = models.BooleanField(default=False)
    # children_ok = models.BooleanField(default=False)
    # dogs_ok = models.BooleanField(default=False)
    # date_posted = models.DateField()

    def get_api_url(self):
        return reverse("api_dog", kwargs={"pk": self.id})

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['adopted']


class Inforrmation(models.Model):
    name = models.CharField(max_length=100, unique=True)
    tile = models.CharField(max_length=100)
    image_url = models.URLField()
    text = models.TextField()


# class Application(models.Model):
#     pass
