import React from 'react';

const useSortableData = (dogs, config = null) => {
  const [sortConfig, setSortConfig] = React.useState(config);

  const sortedDogs = React.useMemo(() => {
    let sortableDogs = [...dogs];
    if (sortConfig !== null) {
      sortableDogs.sort((a, b) => {
        if (a[sortConfig.key] < b[sortConfig.key]) {
          return sortConfig.direction === 'ascending' ? -1 : 1;
        }
        if (a[sortConfig.key] > b[sortConfig.key]) {
          return sortConfig.direction === 'ascending' ? 1 : -1;
        }
        return 0;
      });
    }
    return sortableDogs;
  }, [dogs, sortConfig]);

  const requestSort = (key) => {
    let direction = 'ascending';
    if (
      sortConfig &&
      sortConfig.key === key &&
      sortConfig.direction === 'ascending'
    ) {
      direction = 'descending';
    }
    setSortConfig({ key, direction });
  };

  return { dogs: sortedDogs, requestSort, sortConfig };
};

const DogList = (props) => {
  const { dogs, requestSort, sortConfig } = useSortableData(props.dogs);
  const getClassNamesFor = (name) => {
    if (!sortConfig) {
      return;
    }
    return sortConfig.key === name ? sortConfig.direction : undefined;
  };
    return (
      <div className="my-5 container">
        <h1>Dogs</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>
                <button type="button" onClick={() => requestSort('name')} className={getClassNamesFor('name')}>
                  Name
                </button>
              </th>
              <th>
                <button type="button" onClick={() => requestSort('breed')} className={getClassNamesFor('breed')}>
                  Breed
                </button>
              </th>
              <th>
                <button type="button" onClick={() => requestSort('age')} className={getClassNamesFor('age')}>
                  Age
                </button>
              </th>
              <th>
                <button type="button" onClick={() => requestSort('weight')} className={getClassNamesFor('weight')}>
                  Weight
                </button>
              </th>
              <th>
                <button type="button" onClick={() => requestSort('adopted')} className={getClassNamesFor('adopted')}>
                  Adopted
                </button>
              </th>
            </tr>
          </thead>
            <tbody>
              {dogs.map(dogs => {
                return (
                  <tr key={dogs.id}>
                    <td>{dogs.name}</td>
                    <td>{dogs.breed}</td>
                    <td>{dogs.age}</td>
                    <td>{dogs.weight}</td>
                    <td>{dogs.adopted ? "yes" : "no"}</td>
                  </tr>
                )
              })}
            </tbody>
        </table>
      </div>
    );
};

export default DogList;
