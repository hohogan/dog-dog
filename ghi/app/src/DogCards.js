import React from 'react';

function DogMaster(props) {
    return (
        <div className='col-sm'>
            {props.list.map(dogs => {
                if (dogs.adopted === false)
                return (
                    <div>
                        <div key={dogs.id} className="card mb-4 shadow">
                            <a href={dogs.facebook_url}><img src={dogs.picture_url} className="card-img-top" alt=""></img></a>
                            <h3 className="card-header">{dogs.name}</h3>
                            <ul className="list-group list-group-flush">
                                <li className="list-group-item text-muted">{dogs.breed} | {dogs.age} | {dogs.weight} lbs.</li>
                                <li className="list-group-item text-muted">Other dogs | Children </li>
                                <li className="list-group-item"><a href="#" className="btn btn-primary">Adopt!</a></li>
                            </ul>
                        </div>
                    </div>
                );
                else return (
                    <div>
                        <div key={dogs.id} className="card mb-4 shadow">
                            <a href={dogs.facebook_url}><img src={dogs.picture_url} className="card-img-top" alt=""></img></a>
                            <h3 className="card-header">{dogs.name}</h3>
                            <ul className="list-group list-group-flush">
                                <li className="list-group-item text-muted">{dogs.breed} | {dogs.age} | {dogs.weight} lbs.</li>
                                <li className="list-group-item text-muted">Other dogs | Children </li>
                                <li className="list-group-item"><button type="button" className="btn btn-success" disabled>Adopted!</button></li>
                            </ul>
                        </div>
                    </div>
                );
            }
            )}
        </div>
    );
}

class DogCards extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dogsColumns: [[], [], []]
        };
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/dogs/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                const requests = [];
                for (let dogs of data.dogs) {
                    const detailUrl = `http://localhost:8100/api/dogs/${dogs.id}`;
                    requests.push(fetch(detailUrl));
                }

                const responses = await Promise.all(requests);
                const dogsColumns = [[], [], []]

                let i = 0;
                for (const dogResponse of responses) {
                    if (dogResponse.ok) {
                        const details = await dogResponse.json();
                        dogsColumns[i].push(details)
                        i += 1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(dogResponse);
                    }
                }

                this.setState({ dogsColumns: dogsColumns })
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <>
                <h1 className="center mx-5 mt-4">Available dogs:</h1>
                <div className="container text-center col-sm">
                    <br></br>
                    <div className="row">
                        {this.state.dogsColumns.map((list, index) => {
                            return (
                                <DogMaster key={index} list={list} />
                            );
                        })}
                    </div>
                </div>
            </>
        )
    }
}

export default DogCards;
