import Typical from 'react-typical';

function Cats() {
    setTimeout(function() {
        window.location.replace('http://localhost:3000/dogs/');
      }, 10000);

    return (
        <div className="px-4 py-5 mx-1 my-5 text-center">
            <h1>You took a wrong turn.</h1>
            <h2><Typical

                    loop={Infinity}
                    wrapper="b"
                    steps={[
                        // 'What are you doing?',
                        // 1000,
                        // 'Why are you here?',
                        // 1000,
                        'This is for dogs.',
                        1000,
                        'You want a dog.',
                        1000,
                        'Go get a dog.',
                        1000,
                        'Here let me help.',
                        1000,
                    ]}
            />
            </h2>
        </div>
    );
};

export default Cats;
