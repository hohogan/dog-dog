import React from 'react';
import "./index.css"

const Footer = () => {
    return(
        <div className="main-footer bg-dark text-center">
            <div className="container-fluid">
                <div className="row"><div className="col-sm">&copy;{new Date().getFullYear()} DogDog | Some rights reserved</div>
                    <div>
                        <button type="button" className="btn btn-link"><a href="https://www.facebook.com/JindoLoveRescue"><i className="bi bi-facebook"></i></a></button>
                        <button type="button" className="btn btn-link"><a href="https://www.instagram.com/jindoloverescue/"><i className="bi bi-instagram"></i></a></button>
                        <button type="button" className="btn btn-link"><a href="https://www.tiktok.com/@jindoloverescue?is_from_webapp=1&sender_device=pc"><i className="bi bi-tiktok"></i></a></button>
                        <button type="button" className="btn btn-link"><a href="https://twitter.com/JAdoptions"><i className="bi bi-twitter"></i></a></button>
                        <button type="button" className="btn btn-link"><a href="https://www.youtube.com/@jindoloverescue5764"><i className="bi bi-youtube"></i></a></button>
                        <button type="button" className="btn btn-link"><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=B8XCTEMXYM3VA&source=url"><i className="bi bi-paypal"></i></a></button>
                        <button type="button" className="btn btn-link"><a href="https://www.gitlab.com/hohogan/"><i className="bi bi-github"></i></a></button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer;
