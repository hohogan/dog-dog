import ImageSlider from "./ImageSlider";

function MainPage() {

  const slides = [
    { url: "https://i.imgur.com/DT2IJIG.jpg", title: "slide0" },
    { url: "https://i.imgur.com/QHXTuwK.jpg", title: "slide1" },
    { url: "https://i.imgur.com/DUrMToz.jpg", title: "slide2" },
    { url: "https://i.imgur.com/QzDwOKS.jpg", title: "slide3" },
    { url: "https://i.imgur.com/NFbWAJU.jpg", title: "slide4" },
    { url: "https://i.imgur.com/mIYt3Fe.jpg", title: "slide5" },
    { url: "https://i.imgur.com/aFRsJ9Q.jpg", title: "slide6" },
    { url: "https://i.imgur.com/gylmSbY.jpg", title: "slide7" },
    { url: "https://i.imgur.com/zZZOnD7.jpg", title: "slide8" },
    { url: "https://i.imgur.com/45NirQE.jpg", title: "slide9" },
  ];

  const containerStyles = {
    width: '90%',
    height: '500px',
    margin: '0 auto',
  };

  return (
    <div className="px-4 py-5 mx-1 my-5 text-center">
      <div style={containerStyles}>
        <ImageSlider slides={slides} />
      </div><br></br><br></br>
      <h1 className="display-5 fw-bold">DogDog</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          For the pups.
        </p>
      </div>

    </div>
  );
}

export default MainPage;
