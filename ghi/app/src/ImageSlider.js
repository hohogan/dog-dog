import { useState } from "react";

const ImageSlider = ({ slides }) => {
    const [currentIndex, SetCurrentIndex] = useState(0);

    const sliderStyles = {
        height: "100%",
        position: "relative",
    };

    const slideStyles = {
        width: "100%",
        height: "100%",
        borderRadius: "10px",
        backgroundPosition: "center",
        backgroundSize: "cover",
        backgroundImage: `url(${slides[currentIndex].url})`,
    };

    const leftArrow = {
        position: "absolute",
        top: "50%",
        transform: "translate(0, -50%)",
        left: "32px",
        fontSize: "45px",
        color: "#fff",
        zIndex: 1,
        cursor: "pointer",
    };

    const rightArrow = {
        position: "absolute",
        top: "50%",
        transform: "translate(0, -50%)",
        right: "32px",
        fontSize: "45px",
        color: "#fff",
        zIndex: 1,
        cursor: "pointer",
    };

    const dotsContainer = {
        display: "flex",
        justifyContent: "center",
    };

    const dotStyles = {
        margin: "0 3px",
        cursor: "pointer",
        fontSize: "20px",
    };

    const goToPrevious = () => {
        const isLastSlide = currentIndex === slides.length -1;
        const newIndex = isLastSlide ? 0 : currentIndex + 1;
        SetCurrentIndex(newIndex);
    };

    const goToNext = () => {
        const isFirstSlide = currentIndex === 0;
        const newIndex = isFirstSlide ? slides.length - 1: currentIndex - 1;
        SetCurrentIndex(newIndex);
    };

    const goToSlide = (slideIndex) => {
        SetCurrentIndex(slideIndex);
    }

    return (
    <div style={sliderStyles}>
        <div style={leftArrow} onClick={goToPrevious}>❰</div>
        <div style={rightArrow} onClick={goToNext}>❱</div>
        <div style={slideStyles}></div>
        <div style={dotsContainer}>
            {slides.map((slide, slideIndex) => (
                <div style={dotStyles} key={slideIndex} onClick={() => goToSlide(slideIndex)}>●</div>
            ))}
        </div>
    </div>
    );
};

export default ImageSlider;
