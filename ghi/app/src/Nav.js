import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">DogDog</NavLink>
        <div className="dropdown">
          <button className="btn btn-light dropdown-toggle me-2" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Available Dogs
            </button>
            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <NavLink className="dropdown-item" aria-current="page" to="/dogs/">Dogs</NavLink>
              <NavLink className="dropdown-item" aria-current="page" to="/doglist/">Dog List</NavLink>
              <div className='dropdown-divider'></div>
              <NavLink className="dropdown-item" aria-current="page" to="/cats/">Cats</NavLink>
            </div>
            <NavLink className="btn btn-light me-2" style={{color:"black"}} aria-current="page" to="/contact/">Contact Us</NavLink>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
