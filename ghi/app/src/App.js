import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useEffect, useState } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import Cats from './Cats';
import Footer from './Footer';
import DogList from './DogList';
import DogCards from './DogCards';
import Contact from './Contact';
// import Adoption from './Adoption';
// import Information from './Information';
// import Testimonials from './Testimonials';


function App() {

  const [dogs, setDogs] = useState([]);
  // const [information, setInformation] = useState([]);
  // const [Testimonials, seTestimonials] = useState([]);

  async function getDogs() {
    const url = 'http://localhost:8100/api/dogs/'
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json();
      setDogs(data.dogs)
    }
  }

  // async function getInformation() {
  //   const url = 'http://localhost:8100/api/information/'
  //   const response = await fetch(url)
  //   if (response.ok) {
  //     const data = await response.json();
  //     setInformation(data.information)
  //   }
  // }

    // async function GetTestimonials() {
  //   const url = 'http://localhost:8100/api/testimonials/'
  //   const response = await fetch(url)
  //   if (response.ok) {
  //     const data = await response.json();
  //     setTestimonials(data.information)
  //   }
  // }


  useEffect(() => {
    getDogs();
    // getInformation();
    // getTestimonials();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="page-container">
      <div className="content-wrap">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='contact/' element={<Contact />} />
          <Route path='cats/' element={<Cats />} />
          <Route path="dogs/" element={<DogCards dogs={dogs} />} />
          <Route path="doglist/" element={<DogList dogs={dogs} />} />
          {/* <Route path="adoption/" element={<Adoption />} /> */}
          {/* <Route path="/information" element={<Information />} />
          <Route path='/testimonials/' element={<Testimonials />} /> */}
        </Routes>
        </div>
        <Footer />
      </div>
    </BrowserRouter>
  );
}

export default App;
