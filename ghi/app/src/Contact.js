import React, { useRef } from 'react';
import emailjs from '@emailjs/browser';

const Contact = () => {
        const form = useRef();

        const sendEmail = (e) => {
            e.preventDefault();

            emailjs.sendForm('service_3en0cpn', 'template_9pqvgbi', form.current, 'aXM8yN87s_le1RMqe')
              .then((result) => {
                  console.log(result.text);
              }, (error) => {
                  console.log(error.text);
              }
            );
            e.target.reset()
          };

    return (
    <div className="my-5 container">
        <h1 className="">Contact Us</h1>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-5 mt-5">
                <h1>General Inquiries</h1><br></br>
                <form ref={form} className="form-group" onSubmit={sendEmail} >
                    <div className="form-floating mb-3">
                    <input placeholder="user_name" required type="text" name="user_name" id="user_name" className="form-control" />
                        <label htmlFor="user_name">Full Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input placeholder="user_email" required type="user_email" name="user_email" id="user_email" className="form-control" />
                        <label htmlFor="user_email">Email</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input placeholder="subject" required type="subject" name="subject" id="subject" className="form-control" />
                        <label htmlFor="subject">Subject</label>
                    </div>
                    <div className="form-floating mb-3">
                        <textarea name="message" required type="message" id="message" cols="46" rows="10"></textarea>
                    </div>
                    <button type="submit" className='btn btn-secondary'>Send Message</button>
                </form>
                </div>
            </div>

            {/* <div className="offset-3 col-6">
                <div className="shadow p-5 mt-5">
                <h2>Volunteering / Donate</h2><br></br>
                <form ref={form} className="form-group" onSubmit={sendEmail} >
                    <div className="form-floating mb-3">
                    <input placeholder="user_name" required type="text" name="user_name" id="user_name" className="form-control" />
                        <label htmlFor="user_name">Full Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input placeholder="user_email" required type="user_email" name="user_email" id="user_email" className="form-control" />
                        <label htmlFor="user_email">Email</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input placeholder="subject" required type="subject" name="subject" id="subject" className="form-control" />
                        <label htmlFor="subject">Subject</label>
                    </div>
                    <div className="form-floating mb-3">
                        <textarea name="message" required type="message" id="message" cols="46" rows="10"></textarea>
                    </div>
                    <button type="submit" className='btn btn-secondary'>Send Message</button>
                </form>
                </div>
            </div>  */}

        </div>
    </div>
    )
};

export default Contact;
